"""
Grocery assistant.
Note, it may be only 0-30 apples.
If other quantity is required, the answer is "Столько нет"
"""

from termcolor import colored, cprint


def main():
    """Add "яблоко" word."""
    n = int(input("Сколько яблок Вам нужно?\n"))
    if n < 0 or n > 30:
        print("Столько нет")
    elif n in [1, 21]:
        print("Пожалуйста,", n, "яблоко")
    elif n in [2, 3, 4, 22, 23, 24]:
        print("Пожалуйста,", n, "яблока")
    else:
        print("Пожалуйста,", n, "яблок")


if __name__ == "__main__":
    print(colored("Grocery assistant", "green"))  # color this caption
    main()

